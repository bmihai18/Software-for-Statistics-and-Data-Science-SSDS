import math
import numpy as np
import matplotlib.pyplot as plt

#functie incarcare imagine deteriorata
def load_image(filename):
    # citire imagine png intr-o matrice numpy
    my_img = plt.imread(filename)
    #convertire RGB in tonuri de gri
    img_gray = np.dot(my_img[..., :3], [0.2989, 0.5870, 0.1140])
    #rescalarea pixelilor in {-1,1}
    img_gray = np.where(img_gray > 0.5, 1, -1)
    # paddings = 0 (ajuta la luarea in considerare a marginilor atunci cand analizam vecinii pixelilor)
    img_padded = np.zeros([img_gray.shape[0] + 2, img_gray.shape[1] + 2])
    img_padded[1:-1, 1:-1] = img_gray
    return img_padded

#functia de esantionare (rand, coloana, imaginea reconstituita Y, imaginea deteriorata X)
def sample_y(i, j, Y, X):
    #cauta vecinii pixelului yij
    markov_blanket = [Y[i - 1, j], Y[i, j - 1], Y[i, j + 1], Y[i + 1, j], X[i, j]]
    w = ITA * markov_blanket[-1] + BETA * sum(markov_blanket[:4])
    #calculeaza probabilitatea conditionata de vecinii lui yij
    prob = 1 / (1 + math.exp(-2*w))
    return (np.random.rand() < prob) * 2 - 1

#functia de obtinere a distributiei posterioare
def get_posterior(filename, burn_in_steps, total_samples):
    #incarcare imagine deteriorata X
    X = load_image(filename)
    posterior = np.zeros(X.shape)
    print(X.shape)
    #initializare random a imaginii reconstituite Y
    Y = np.random.choice([1, -1], size=X.shape)
    for step in range(burn_in_steps + total_samples):
        #iterare peste i, j pentru esantionarea fiecarui pixel yij
        for i in range(1, Y.shape[0]-1):
            for j in range(1, Y.shape[1]-1):
                y = sample_y(i, j, Y, X)
                #actualizare imagine reconstituita Y cu valoarea esantionata yij
                Y[i, j] = y
                if y == 1 and step >= burn_in_steps:
                    posterior[i, j] += 1
    posterior = posterior / total_samples
    return posterior

#functia de eliminare a zgomotului din imagine
def denoise_image(filename, burn_in_steps, total_samples):
    #apeleaza functia get_posterior pentru obtinerea probabilitatilor posterioare p(Y=1|Y_neighbor)
    posterior = get_posterior(filename, burn_in_steps, total_samples)
    denoised = np.zeros(posterior.shape, dtype=np.float64)
    #setam pragul de probabilitate posterioara la 0.5 si obtinem imaginea Y din distributia posterioara
    denoised[posterior > 0.5] = 1
    return denoised[1:-1, 1:-1]

#functie salvare imagine (conversie matrice numpy in imagine fara zgomot .png)
def save_image(denoised_image):
    plt.imshow(denoised_image, cmap='gray')
    plt.title("denoised image")
    plt.savefig('denoise_image.png')
    plt.close()


if __name__ == '__main__':
    #Hiperparametri ita, beta
    ITA = 1
    BETA = 1
    #total esantionane
    total_samples = 1500
    #pasi de "ardere"
    burn_in_steps = 100
    #imagine reconstituita
    denoised_img = denoise_image("img_noisy.png", burn_in_steps=burn_in_steps,
                                 total_samples=total_samples)
    save_image(denoised_img)