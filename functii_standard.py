import numpy as np
import pandas as pd
from pandas.api.types import is_numeric_dtype

def nan_replace_t(t):
    assert isinstance(t, pd.DataFrame)
    for v in t.columns:
        if any(t[v].isna()):
            if is_numeric_dtype(t[v]):
                t[v].fillna(t[v].mean(), inplace=True)
            else:
                t[v].fillna(t[v].mode()[0], inplace=True)


def nan_replace_m(x):
    assert isinstance(x, np.ndarray)
    k = np.where(np.isnan(x))
    x[k] = np.nanmean(x[:, k[1]], axis=0)
