import numpy as np


class mat3d():
    def __init__(self, mat=None):
        if mat is None:
            self.__t = np.identity(4)
        else:
            if not isinstance(mat, np.ndarray):
                raise Exception("Paramentrul mat nu este de tip masiv!")
            n, m = mat.shape
            if n != 4 or m != 4:
                raise Exception("Shape incorect!")
            self.__t = mat

    def __str__(self):
        out = ""
        n = np.shape(self.__t)[0]
        for i in range(n):
            out += ",".join([str(v) for v in self.__t[i, :]])
            out += "\n"
        return out

    @property
    def t(self):
        return self.__t

    def set_element(self, i, j, v):
        if i not in range(4):
            raise Exception("Indice linie nepermisa")
        if j not in range(3):
            raise Exception("Indice coloana nepermisa")
        self.__t[i, j] = v

    def multiply(self, other):
        if not isinstance(other, mat3d):
            raise Exception("Matrice nepermisa in compunere")
        m = self.__t @ other.__t
        return mat3d(m)

    def transform(self, x):
        if not isinstance(x, (tuple, list, np.ndarray)):
            raise Exception("Tip nepermis pentru punctul x!")
        if len(x) != 4:
            raise Exception("Numar nepermis de elemente!")
        x = np.array(x)
        return x @ self.__t


class mattran3d(mat3d):
    def __init__(self, a, b, c):
        super().__init__()
        self.set_element(3, 0, a)
        self.set_element(3, 1, b)
        self.set_element(3, 2, c)


class matscal3d(mat3d):
    def __init__(self, a, b, c):
        super(matscal3d, self).__init__()
        self.set_element(0, 0, a)
        self.set_element(1, 1, b)
        self.set_element(2, 2, c)
