import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram
from scipy.stats import shapiro, kstest, entropy
from pandas.api.types import is_numeric_dtype
from scipy.stats import entropy
from sklearn.metrics import confusion_matrix, cohen_kappa_score
from seaborn import heatmap, kdeplot, scatterplot


def inlocuire_nan(x):
    k = np.where(np.isnan(x))
    if len(k[0]) != 0:
        x[k] = np.nanmean(x[:, k[1]], axis=0)
    else:
        print("Nu exista valori lipsa!")


def standardizare(x, scal=True, nlib=0):
    x_ = x - np.mean(x, axis=0)
    if scal:
        x_ = x_ / np.std(x, axis=0)
    return x_


def salvare(x, nume_linii=None, nume_coloane=None, nume_fisier="out.csv"):
    pd.DataFrame(data=x, index=nume_linii, columns=nume_coloane).to_csv(nume_fisier)


def teste_c(x, p_value=0.05):
    assert isinstance(x, np.ndarray)
    m = x.shape[1]
    t = np.zeros(shape=(m, 4))
    t_test = np.empty(shape=(m, 2), dtype=bool)
    for j in range(m):
        t[j, 0:2] = shapiro(x[:, j])
        t[j, 2:] = kstest(x[:, j], "norm")
        t_test[j, 0] = t[j, 1] > p_value
        t_test[j, 1] = t[j, 3] > p_value
    return t,t_test


def ponderi(t):
    assert isinstance(t, pd.DataFrame)
    x = t.values
    p = (x.T / np.sum(x, axis=1)).T
    return pd.DataFrame(p, t.index, t.columns)

def f_shannon(t):
    return entropy(t,base=2)

def f_simpson(t):
    p = t.values
    return 1-np.sum(p*p)

def f_isimpson(t):
    p = t.values
    return 1/np.sum(p*p)
def disimilaritate(t):
    assert isinstance(t, pd.DataFrame)
    x = t.iloc[:, :-1].values
    s = np.sum(x, axis=1)
    r = (s - x.T).T
    tx = np.sum(x, axis=0)
    tr = np.sum(r, axis=0)
    tx[tx == 0] = 1
    tr[tr == 0] = 1
    d = 0.5 * np.sum(np.abs(x / tx - r / tr), axis=0)
    return pd.Series(d, t.columns[:-1])



def medie_ponderata(t, coloane_calcul, coloana_pondere):
    assert isinstance(t, pd.DataFrame)
    x = t[coloane_calcul].values
    w = t[coloana_pondere].values
    medii = np.average(x, axis=0, weights=w)
    return pd.Series(medii, coloane_calcul)


def g_corelograma(t, titlu):
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    assert isinstance(ax, plt.Axes)
    ax.set_title(titlu, fontdict={"fontsize": 18, "color": "b"})
    heatmap(t, vmin=-1, vmax=1, cmap="RdYlBu", annot=True, ax=ax)
    plt.show()


def g_linii(t, titlu):
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    assert isinstance(ax, plt.Axes)
    ax.set_title(titlu, fontdict={"fontsize": 18, "color": "b"})
    for activitate in t.columns:
        ax.plot(t[activitate], label=activitate)
    ax.legend()
    plt.show()


def harta(gdf, camp_harta):
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    assert isinstance(ax, plt.Axes)
    ax.set_title("Harta - " + camp_harta, fontdict={"fontsize": 18, "color": "b"})
    gdf.plot(column=camp_harta, cmap="RdYlBu", legend=True, ax=ax)
    plt.show()


def nan_replace(t):
    assert isinstance(t, pd.DataFrame)
    for v in t.columns:
        if any(t[v].isna()):
            if is_numeric_dtype(t[v]):
                t[v].fillna(t[v].mean(), inplace=True)
            else:
                t[v].fillna(t[v].mode()[0], inplace=True)





def shannon_waever(t):
    x = t.iloc[:, :-1].values
    s = np.sum(x, axis=0)
    s[s == 0] = 1
    p = x / s
    p[p == 0] = 1
    h = entropy(p, base=2, axis=0)
    return pd.Series(h, t.columns[:-1])

def distributie(z,y,k=0):
    fig=plt.figure(figsize=(9,6))
    ax = fig.add_subplot(1,1,1)
    ax.set_title('Disributie in axa discriminanta '+str(k+1))
    kdeplot(x=z[:,k],hue=y,fill=True,ax=ax)
    plt.show()

def calcul_acuratete(y,y_,clase):
    c = confusion_matrix(y, y_)
    tc = pd.DataFrame(c,clase,clase)
    tc['acuratete']=np.diag(c)*100/np.sum(c,axis=1)
    acuratete_medie = tc['acuratete'].mean()
    acuratete_globala = sum(np.diag(c))*100/len(y)
    index_kc = cohen_kappa_score(y,y_)
    acuratete = pd.Series([acuratete_medie,acuratete_globala,index_kc],['Acuratete Medie', 'Acuratete Globala', 'Index'], name='Acuratete')
    return tc,acuratete
def show():
    plt.show()

def scattterplot_clase(z,y,k1=0,k2=1):
    fig=plt.figure(figsize=(9,6))
    ax = fig.add_subplot(1,1,1,aspect=1)
    ax.set_title('Plot instante in axele discriminante')
    ax.set_xlabel("Z"+str(k1+1))
    ax.set_ylabel("Z"+str(k2+1))
    scatterplot(x=z[:,k1],y=z[:,k2],hue=y,ax=ax)


# Functie care salveaza erorile de clasificare pe setul de testare
def err(y,y_,tinta,etichete,eticheta_model):
    tabel_clasificare = pd.DataFrame(
        data={
            tinta: y,
            "Predictie": y_
        }, index=etichete
    )
    tabel_clasificare[y != y_].to_csv("Err_"+eticheta_model+".csv")
def plot_ierarhie(h,threshold,etichete):
    fig = plt.figure(figsize=(9, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title("Plot ierarhie (Dendrograma)")
    dendrogram(h,ax=ax,color_threshold=threshold,labels=etichete)

def histograme(z,p,variabila):
    fig = plt.figure(figsize=(9, 6))
    clase = np.unique(p)
    q = len(clase)
    fig.suptitle("Histograme pentru variabila "+variabila)
    axe = fig.subplots(1,q,sharey=True)
    for i in range(q):
        axe[i].set_xlabel(clase[i])
        axe[i].hist(x=z[p==clase[i]],range=(min(z),max(z)),rwidth=0.9)


def show():
    plt.show()
