from sklearn.cluster import AgglomerativeClustering
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler

from functii import *
from functii_standard import nan_replace_t

tabel = pd.read_csv("wine.csv",
                    index_col=0)
x = tabel.values
print(x,type(x))
inlocuire_nan(x)
print(x)

x_c = standardizare(x, scal=False)
z = standardizare(x)
salvare(x_c, tabel.index, tabel.columns, "CalitateVin_c.csv")
salvare(z, tabel.index, tabel.columns, "CalitateVin_z.csv")

standardScaler = StandardScaler()
z_ = standardScaler.fit_transform(x)

# Calcul corel/covar
r = np.corrcoef(x, rowvar=False)
v = np.cov(x, rowvar=False)
salvare(r, tabel.columns, tabel.columns, "r.csv")
salvare(v, tabel.columns, tabel.columns, "v.csv")
r_ = np.triu(r, k=1)
k = np.where(np.abs(r_) > 0.4)
print(k)
variabile = np.array(tabel.columns)
t_r = pd.DataFrame(data={
    "V1": variabile[k[0]],
    "V2": variabile[k[1]],
    "R": r_[k]
})
print(t_r)
t_r.to_csv("r_.csv", index=False)
rez_teste = teste_c(x)
print(rez_teste)
salvare(rez_teste[1], tabel.columns,
        ["Shapiro", "Kolmogorov-Smirnov"],
        "teste_c.csv")

t_vinuri = pd.read_csv("wine.csv",index_col=0)
nan_replace(t_vinuri)

comp_vin = list(t_vinuri)[1:]

# Grupare Alcool
calit = pd.read_csv("winequality_red.csv", index_col=0)
t_calit = tabel.merge(right=calit, left_index=True, right_index=True)
t_calit_alcool = t_calit[comp_vin + ["alcohol"]].groupby(by="alcohol").agg(sum)
t_calit_alcool.to_csv("calitate-rosu.csv")



# Disim/Shannon
t_disimilaritate = t_calit[comp_vin+["alcohol"]].groupby(by="alcohol").apply(func=disimilaritate)
t_disimilaritate.to_csv("disimilaritate.csv")
#
t_shannon = t_calit[comp_vin+["alcohol"]].groupby(by="alcohol").apply(func=shannon_waever)
t_shannon.to_csv("shannon.csv")




ds = pd.read_csv("wine.csv", index_col=0)
nan_replace_t(ds)

variabile = list(ds)[1:]

x = ds[variabile].values
n = x.shape[0]
nr_jonctiuni = n - 1

k = 3
model_hclust = AgglomerativeClustering(k, compute_distances=True)
model_hclust.fit(x)

h = np.zeros((n - 1, 4))
h[:, 0] = model_hclust.children_[:, 0]
h[:, 1] = model_hclust.children_[:, 1]
h[:, 2] = model_hclust.distances_
nr_jonctiune = nr_jonctiuni - k
threshold = (h[nr_jonctiune, 2] + h[nr_jonctiune + 1, 2]) / 2
plot_ierarhie(h, threshold, ds.index)

partitie_3 = np.array(["c" + str(i + 1) for i in model_hclust.labels_])
print(partitie_3)

tabel_partitii = pd.DataFrame(data={
    "P3": partitie_3
}, index=ds.index)
print(tabel_partitii)

for i in range(len(variabile)):
    histograme(x[:, i], partitie_3, variabile[i])

show()


tabel_invatare_testare= pd.read_csv("wine.csv" , index_col=0)

variabile = list(tabel_invatare_testare)

predictori=variabile[:-1]
tinta=variabile[-1]

x_train,x_test,y_train,y_test =train_test_split(tabel_invatare_testare[predictori],tabel_invatare_testare[tinta],test_size=0.4)

model_lda = LinearDiscriminantAnalysis()
model_lda.fit(x_train,y_train)

clase = model_lda.classes_

q = len(clase)-1
z = model_lda.transform(x_train)

for i in range(q):
    distributie(z,y_train,i)

#testare
predict_lda_test = model_lda.predict(x_test)

ac_lda = calcul_acuratete(y_test,predict_lda_test,clase)

ac_lda[0].to_csv('mat_conf_lda.csv')
ac_lda[1].to_csv('acuratete_lda.csv')


#aplicarea modelului pe setul de aplicat

x_apply = pd.read_csv('wine_apply.csv',index_col=0)

pred_lda = model_lda.predict(x_apply)

tabel_predictii = pd.DataFrame(data={
    'Predictie lda':pred_lda
    },index=x_apply.index)

#creare model bayesian
model_bayes = GaussianNB()
model_bayes.fit(x_train,y_train)

predict_bayes_test = model_bayes.predict(x_test)

acuratete_bayes = calcul_acuratete(y_test,predict_bayes_test,clase)

acuratete_bayes[0].to_csv('mat_conf_bayes.csv')
acuratete_bayes[1].to_csv('acuratete_bayes.csv')

